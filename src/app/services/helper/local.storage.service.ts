import { Injectable } from '@angular/core';
import { BaseHttpService } from '../http/base-http.service';

@Injectable({
    providedIn: 'root'
  })

  export class LocalStorage{
      
    constructor(){}

    setData(key,data){
        localStorage.setItem(key,JSON.stringify(data));
    }

    getData(key){
        var data=localStorage.getItem(key);
        return data?data:null;
    }

  }