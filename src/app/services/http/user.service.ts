import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BaseHttpService } from 'src/app/services/http/base-http.service';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService extends BaseHttpService {

  constructor(http: HttpClient) {
    super(http);
  }

  login(user) {
    var url = this.baseUrl + "login";
    return this.post(url,user);
  }

  register(user){
    var url = this.baseUrl + "register";
    return this.post(url,user);
  }

  getUsers(page=1){
    var url = this.baseUrl + "users?page="+page;
    return this.get(url);
  }

  getUserById(id){
    var url = this.baseUrl + "users/"+id;
    return this.get(url);
  }

  addUser(user){
    var url = this.baseUrl + "users";
    return this.post(url,user);
  }

  updateUser(id,user){
    var url = this.baseUrl + "users/"+id;
    return this.update(url,user);
  }

  deleteUser(id){
    var url = this.baseUrl + "users/"+id;
    return this.delete(url);
  }

  

  



}
