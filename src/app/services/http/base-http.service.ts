import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { catchError } from "rxjs/operators";
import { of } from 'rxjs/internal/observable/of';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class BaseHttpService {

  baseUrl = environment.apiUrl;

  constructor(private http: HttpClient) { }

  handleError(error) {
    switch (error.status) {
      case 400:
        return error.error;
      default:
        return "some thing wrong"
    }
  }

  get(url) {
    return this.http.get(url).pipe(
      catchError((error) => {
        return of(this.handleError(error));
      })
    );
  }

  post(url, body) {
    return this.http.post(url, body).pipe(
      catchError((error) => {
        return of(this.handleError(error));
      })
    );
  }

  update(url, body) {
    return this.http.put(url, body).pipe(
      catchError((error) => {
        return of(this.handleError(error));
      })
    );
  }

  delete(url) {
    return this.http.delete(url).pipe(
      catchError((error) => {
        return of(this.handleError(error));
      })
    );
  }

}
