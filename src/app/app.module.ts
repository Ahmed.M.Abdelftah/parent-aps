import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { LoginComponent } from './pages/login/login.component';
import { UserListComponent } from './pages/user-list/user-list.component';
import { AddUserComponent } from './pages/add-user/add-user.component';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from 'src/app/app.routing.module';
import { FormsModule } from '@angular/forms';
import { UserService } from 'src/app/services/http/user.service';
import { RouterModule } from '@angular/router';
import {NgxPaginationModule} from 'ngx-pagination'; // <-- import the module
import { LoadingBarHttpClientModule } from '@ngx-loading-bar/http-client';
import { SpinnerComponent } from './components/spinner/spinner.component';
import { DeletePopupComponent } from './pages/delete-popup/delete-popup.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    UserListComponent,
    AddUserComponent,
    SpinnerComponent,
    DeletePopupComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule,
    RouterModule,
    NgxPaginationModule,
    LoadingBarHttpClientModule
  ],
  providers: [
    UserService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
