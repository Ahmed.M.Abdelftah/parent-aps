import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/http/user.service';
import { ActivatedRoute } from '@angular/router';
import { repeat } from 'rxjs/operators';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit {

  user={name:"",job:""};
  errorMessage="";
  userId=false;
  loading=false;
  userAvatar;
  successMessage="";

  constructor(private userService:UserService,private activatedRoute:ActivatedRoute) { 
    
  }

  ngOnInit() {
   // this.editUser(1);
  }


  addNewUser(){
    this.loading=true;
    if(!this.userId){
      this.userService.addUser(this.user).subscribe(response=>{
        this.loading=false;
        if(response.error){
          this.errorMessage=response.error;
        }else{
          this.successMessage="User Added Successfully";
        }
      });
    }else{
      this.userService.updateUser(this.userId,this.user).subscribe(response=>{
        this.loading=false;
        if(response.error){
          this.errorMessage=response.error;
        }else{
          this.successMessage="User Updated Successfully";
        }
      });
    }
    

  }

  editUser(id){
    this.loading=true;
    this.userId=id;
    this.user={name:"",job:""};
    this.userAvatar=false;
    this.successMessage="";
    this.errorMessage="";
    (<any>$("#user-data-modal")).modal();
    this.userService.getUserById(id).subscribe(response=>{
      this.loading=false;
      if(response.error){
        this.errorMessage=response.error;
      }else{
        this.user.name=response.data.first_name+" "+response.data.last_name;
        this.userAvatar=response.data.avatar;
      }
    })
  }

  addUser(){
    this.userId=false;
    this.user={name:"",job:""};
    this.successMessage="";
    this.errorMessage="";
    (<any>$("#user-data-modal")).modal();
  }


  

}
