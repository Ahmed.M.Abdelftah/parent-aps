import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/http/user.service';
import { LocalStorage } from '../../services/helper/local.storage.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  user={
    email:"",
    password:""
  }
  errorMessage="";

  constructor(private userService:UserService,private localStorage:LocalStorage,private router:Router) { }

  ngOnInit() {
  }

  login(){
    this.errorMessage="";
    this.userService.login(this.user).subscribe(response=>{
      if(response.error){
        this.errorMessage=response.error;
      }else{
        //handle success behavior
        this.localStorage.setData("token",response);
        this.router.navigate(["/users"]);
      }
    })
  }

}
