import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { UserService } from '../../services/http/user.service';

@Component({
  selector: 'app-delete-popup',
  templateUrl: './delete-popup.component.html',
  styleUrls: ['./delete-popup.component.css']
})
export class DeletePopupComponent implements OnInit {

  id = 0;
  fullName = "";
  errorMessage = "";
  @Output() userDeleted: EventEmitter<Number> = new EventEmitter<Number>();
  loading = false;

  constructor(private userService: UserService) { }

  ngOnInit() {
  }

  show(id, full_name) {
    this.id = id;
    this.fullName = full_name;
    (<any>$("#delete-user-modal")).modal();
  }

  delete() {
    this.loading = true;
    this.userService.deleteUser(this.id).subscribe(response => {
      this.loading = false;
      (<any>$("#delete-user-modal")).modal('hide');
      this.userDeleted.emit(this.id);
    });
  }

}
