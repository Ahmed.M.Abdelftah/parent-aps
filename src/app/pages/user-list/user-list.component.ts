import { Component, OnInit, ViewChild } from '@angular/core';
import { UserService } from '../../services/http/user.service';
import { AddUserComponent } from '../add-user/add-user.component';
import { DeletePopupComponent } from '../delete-popup/delete-popup.component';
import * as _ from "lodash";

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  @ViewChild('addEditUser') addEditUser: AddUserComponent;
  @ViewChild('deletePopup') deletePopupComponent: DeletePopupComponent;
  users = {
    data: [],
    total_pages: 0
  };
  page = 1;
  loading = false;
  showDetailsView = false;
  singleUserIndex;
  singleUser;

  constructor(private userService: UserService) { }

  ngOnInit() {
    this.getUsers(this.page);
  }

  getUsers(page) {
    this.loading = true;
    this.userService.getUsers(page).subscribe(response => {
      this.users.data = this.users.data.concat(response.data);
      this.users.total_pages = response.total_pages;
      this.loading = false;
    });
  }

  getNextPage() {
    this.page++;
    this.getUsers(this.page);
  }

  viewDetails(index) {
    this.singleUser = this.users.data[index];
    this.singleUserIndex = index;
    this.showDetailsView = true;
  }

  hideDetailsView() {
    this.showDetailsView = false;
  }

  editUser(id) {
    this.addEditUser.editUser(id);
  }

  addUser() {
    this.addEditUser.addUser();
  }

  deleteUser(id, name) {
    this.deletePopupComponent.show(id, name);
  }

  userDeleted(id) {
    var index = _.findIndex(this.users.data, ["id", id]);
    this.users.data.splice(index, 1);
    this.showDetailsView=false;
  }

}
