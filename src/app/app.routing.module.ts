import { ExtraOptions, RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { LoginComponent } from './pages/login/login.component';
import { UserListComponent } from './pages/user-list/user-list.component';
import { AddUserComponent } from './pages/add-user/add-user.component';


const routes: Routes = [
    { path: '', component: LoginComponent, pathMatch: 'prefix' },
    { path: 'users', component: UserListComponent, pathMatch: 'prefix' },
    { path: '**', component: LoginComponent },
]


const config: ExtraOptions = {
    useHash: false,
};

@NgModule({
    imports: [RouterModule.forRoot(routes, config)],
    exports: [RouterModule],
})

export class AppRoutingModule {

}